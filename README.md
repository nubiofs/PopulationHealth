![Build Status](https://gitlab.com/smart-city-platform/PopulationHealth/badges/master/build.svg)
[![Mezuro](https://img.shields.io/badge/mezuro-green-green.svg)](http://mezuro.org/en/repositories/126)

---

Population Health API
=====================

This project covers the creation of a web interface that shows to the user the less crowded health facility of a given type near to his. (Full API description available in the [Stoa page](https://social.stoa.usp.br/poo2016/projeto/grupo-1-saude) of this project)

Environment setup
-----------------

We're using the gem 'gmaps4rails' to provide acess to the JS code for GoogleMaps. To create the JS runtime, gem 'therubyracer' is used. Besides, these, other required gems are specified within Gemfile
To get setup to run this application:
* Install rvm, running in the terminal:
  * ```$ rvm install 2.3.1```
* Clone project repository:
  * ```$ git clone git@gitlab.com:smart-city-platform/PopulationHealth.git```
* cd into project dir:
  * ```$ cd PopulationHealth```
* Install bundle and other gems:
  * ```$ gem install bundle```
  * ```$ bundle install```
* Start application:
  * ```$ rails s -p $PORT -b $IP```

Running the tests
-------------------
We are using rspec-rails and Capybara to test the application. To run the tests, just run the following command line, which will run all the tests under the specs directory:
* Run rspec specifications:
  * ```rspec spec```

What's implemented?
-------------------
* Phase 1
  * User interface
    * A map pointing to the facilities addresses given by csv imported from Geosampa website
    * An interface to add new facilities
    * Filtering the places in the map, by their type (among the types provided by Geosampa csv)
  
  * Application core
    * A csv parser on db seeds, to consume data from csv from Geosampa

* Phase 2
  * User interface
    * Build the interface for management data, to get the status of each of the facilities (capacity and number of people waiting)
    * User can input his address to check the it in the map

TODOs (Phase 3)
------------
* Consume sensors data, to get the status of how crowded each of the facilities is
* Implement the algorithm to give the user a route to the recommended facility, based on his search (type of facility, distance from his location etc)
* Consume historical data, stored from sensors and provide numbers in this historical data interface

Useful links
----------
* [Link to project page within Stoa](https://social.stoa.usp.br/poo2016/projeto/grupo-1-saude)
* [Geosampa website](http://geosampa.prefeitura.sp.gov.br/PaginasPublicas/_SBC.aspx)
* [Implementation diagram](https://drive.google.com/open?id=0B7SL-ZKhDqvPeEpvMUU4Rm1zOTg)

Note
----
csv file from Geosampa is available up to 2014. Some of the addresses don't point to exactly location, according to GoogleMaps, however this can be easily fixed, by changing csv file, once it's fixed by Geosampa.

Troubleshooting
---------------
It was experienced errors on running ```$ bundle install``` due to errors on installing 'nokogiri' gem (a dependency for some other gem). So, if this issue is faced, run ```$ bundle config build.nokogiri --use-system-libraries```. This will likely solve the issue.



