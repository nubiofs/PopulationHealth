require "rails_helper"

RSpec.describe HealthFacility, :type => :model do
    
  describe "Data base tests" do
    
    it "Create a new health facility" do
      @facility = HealthFacility.create!(lat: 123, longi: 213, address: "Rua Central", typeOf: "Hospital", classOf: "Pediatria")
      expect(@facility.lat).to eq(123)
      expect(@facility.longi).to eq(213)
      expect(@facility.address).to eq("Rua Central")
        expect(@facility.typeOf).to eq("Hospital")
        expect(@facility.classOf).to eq("Pediatria")
    end
      
    it "Verify if the table is not empty" do
      HealthFacility.create!(lat: 123, longi: 213, address: "Rua Central", typeOf: "Hospital", classOf: "Pediatria")
      @value = HealthFacility.exists?
      expect(@value).to be true
    end
      
    it "Verify if the table is empty" do
      HealthFacility.delete_all
      @value = HealthFacility.exists?
      expect(@value).to be false
    end
    
  end
end

